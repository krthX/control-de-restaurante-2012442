package org.bryampaniagua.sistema;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.geometry.Insets;
import javafx.geometry.Pos;

import org.bryampaniagua.database.Conexion;
import org.bryampaniagua.manejadores.ManejadorUsuario;
import org.bryampaniagua.beans.Usuario;
import org.bryampaniagua.sistema.App;
	/**
		@author BryamPaniagua
		
		Esta clase extiende a 
			javafx.application.Application
		
		e implementa la interfaz
			javafx.event.EventHandler
		
				recibiendo como tipo 
					javafx.event.Event;
					
		Contiene las disposiciones para la creacion de 
		los efectos visuales de la aplicacion. Teniendo 
		metodos como getTabPane() que devuelve el contenedor 
		de Tabs. Tambien contiene las Tabs de los modulos.
		
		Cuenta con el metodo start que recibe una variable 
		del tipo 
			javafx.stage.Stage
		
		que sera nuestra ventana para la aplicacion.
	
	**/
public class Login extends Application implements EventHandler<Event>{
	private Scene primaryScene;
	private TabPane tpPrincipal;
	private Tab tbLogin, tbPrincipal;
	private GridPane gpLogin;
	private TextField tfCorreo;
	private PasswordField pfContrasenia;
	private BorderPane bpPrincipal, bpLogin;
	
	private Conexion cnx;
	private ManejadorUsuario manus;
	private App app;
	
	/**
		Metodo start hace el envio de la variable que contiene la conexion con la base de datos SQL a los metodos set
		de cada manejador creado de manera local en la clase, para tener una misma conexion en toda la aplicacion y 
		ejecutar metodos de los manejadores para la obtencion de datos necesarios.
		
		Crea la escena y hace el llamado al metodo getTabPane()	que devuelve el contenedor de Tabs.	
			@param primaryStage
	**/
	public void start(Stage primaryStage){		
		this.cnx = new Conexion();
		manus = new ManejadorUsuario(cnx);
		
		primaryScene = new Scene(this.getTabPane());
		
		primaryStage.setTitle("Pan & Agua S.A."); 
		primaryStage.setScene(primaryScene);
		primaryStage.show();
	}
	
	/**
		Este es el contenedor y recibe como valor inicial la Tab principal enviada por el metodo getTabPrincipal, este a 
		su vez es agregada en la lista de pestanias y devuelve el contenedor de Pestanias
			@return tpPrincipal;
	**/
	public TabPane getTabPane(){
		if(tpPrincipal == null){
			tpPrincipal = new TabPane();
			
			tpPrincipal.getTabs().add(this.getTabLogin());
		}
		return tpPrincipal;
	}

	/**
		
		Agrega el contenido recibido del metodo getContentLogin	que envia los controles para iniciar sesion en la app
			@return tbLogin
	**/
	public Tab getTabLogin(){
		if(tbLogin == null){
			tbLogin = new Tab("Login");
			
			tbLogin.setContent(this.getContentLogin());
		}
		return tbLogin;
	}
	
	/**
		Es el contenido para la pestania de inicio de sesion almacenada en un GridPane
		@return gpLogin
	**/
	public BorderPane getContentLogin(){
		if(bpLogin == null){
			gpLogin = new GridPane();
			gpLogin.setAlignment(Pos.CENTER);
			
			tfCorreo = new TextField();
			tfCorreo.setPromptText("ej. 'asad@ha.com'");
			tfCorreo.addEventHandler(KeyEvent.KEY_RELEASED, this);
			GridPane.setMargin(tfCorreo, new Insets(3,3,15,25));
			
			pfContrasenia = new PasswordField();
			pfContrasenia.setPromptText("Contrasenia");
			pfContrasenia.addEventHandler(KeyEvent.KEY_RELEASED, this);
			GridPane.setMargin(pfContrasenia, new Insets(3,3,3,25));
			
			gpLogin.add(new Label("Correo Electronico") , 0, 0);
			gpLogin.add(new Label("Contrasenia ") , 0, 1);
			gpLogin.add(tfCorreo, 1, 0);
			gpLogin.add(pfContrasenia, 1, 1, 2, 3);
			
			bpLogin = new BorderPane();
			
			bpLogin.setCenter(new Label("Pan & Agua\nRestaurant & Conference center."));
			bpLogin.setRight(gpLogin);
		}
		return bpLogin;
	}
	
	/**
	
	**/
	public ManejadorUsuario getManus(){
		return manus;
	}
	public Conexion getCnx(){
		return cnx;
	}
	
	public void addTab(Tab tab, boolean limpiar){
		if(limpiar){
			tpPrincipal.getTabs().clear();
		}
		tpPrincipal.getTabs().add(tab);
	}

	/**
		
		Recibe un evento y ejecuta una accion segun el tipo y origen del evento
		@param event
	**/
	public void handle(Event event){
		if(event instanceof KeyEvent){
			KeyEvent keyEvent = (KeyEvent)event;
			if(keyEvent.getCode() == KeyCode.ENTER){
				if(event.getSource().equals(tfCorreo) || event.getSource().equals(pfContrasenia)){
					if(manus.comprobar(tfCorreo.getText().trim(), pfContrasenia.getText().trim()) != false){
						app = new App(this);
						addTab(app.getTabPrincipal(), true);
					}
				}	
			}
		}
	}
}
