package org.bryampaniagua.beans;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.LongProperty;

public class Cliente{
	private StringProperty nombre;
	private IntegerProperty idCliente, pin;
	private LongProperty noTarjeta;
	
	public Cliente(){
		this.idCliente = new SimpleIntegerProperty();
		this.nombre = new SimpleStringProperty();
		this.noTarjeta = new SimpleLongProperty();
		this.pin = new SimpleIntegerProperty();
	}
	public Cliente(int idCliente, String nombre, long noTarjeta, int pin){
		this.idCliente = new SimpleIntegerProperty(idCliente);
		this.nombre = new SimpleStringProperty(nombre);
		this.noTarjeta = new SimpleLongProperty(noTarjeta);
		this.pin = new SimpleIntegerProperty();
	}
	
	public int getIdCliente(){
		return idCliente.get();
	}
	public void setIdCliente(int idCliente){
		this.idCliente.set(idCliente);
	}
	public IntegerProperty idClienteProperty(){
		return idCliente;
	}
	
	public String getNombre(){
		return nombre.get();
	}
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	public StringProperty nombreProperty(){
		return nombre;
	}
	
	public long getNoTarjeta(){
		return noTarjeta.get();
	}
	public void setNoTarjeta(long noTarjeta){
		this.noTarjeta.set(noTarjeta);
	}
	public LongProperty noTarjetaProperty(){
		return noTarjeta;
	}
	
	public int getPin(){
		return pin.get();
	}
	public void setPin(int pin){
		this.pin.set(pin);
	}
	public IntegerProperty pinProperty(){
		return pin;
	}
}