CREATE DATABASE dbRestaurante2012440
GO
USE dbRestaurante2012440
GO
CREATE TABLE TipoDePlatillo(
	idTipoPlatillo	INT IDENTITY(1,1),
	nombre	VARCHAR(255),
	PRIMARY KEY(idTipoPlatillo)
)
CREATE TABLE TipoDeBebida(
	idTipoBebida	INT IDENTITY(1,1),
	nombre	VARCHAR(255),
	PRIMARY KEY(idTipoBebida)
)
CREATE TABLE Platillo(
	idPlatillo	INT IDENTITY(1,1),
	nombre	VARCHAR(255),
	idTipoPlatillo	INT NOT NULL,
	precio	INT,
	PRIMARY KEY(idPlatillo),
	FOREIGN KEY(idTipoPlatillo) REFERENCES TipoDePlatillo(idTipoPlatillo)
)
CREATE TABLE Ingrediente(
	idIngrediente	INT IDENTITY(1,1),
	nombre	VARCHAR(255),
	PRIMARY KEY(idIngrediente)
)
CREATE TABLE DetallePlatillo(
	idDetallePlatillo	INT IDENTITY(1,1),
	idPlatillo	INT NOT NULL,
	idIngrediente	INT NOT NULL,
	cantidad	INT,
	PRIMARY KEY(idPlatillo, idIngrediente),
	FOREIGN KEY(idPlatillo) REFERENCES Platillo(idPlatillo),
	FOREIGN KEY(idIngrediente) REFERENCES Ingrediente(idIngrediente)
)
CREATE TABLE Bebida(
	idBebida	INT IDENTITY(1,1),
	nombre	VARCHAR(255),
	idTipoBebida	INT NOT NULL,
	precio INT,
	PRIMARY KEY(idBebida),
	FOREIGN KEY(idTipoBebida) REFERENCES TipoDeBebida(idTipoBebida)
)
CREATE TABLE Usuario(
	idUsuario	INT IDENTITY(1,1),
	nombre	VARCHAR(255),
	nick	VARCHAR(255),
	contrasenia	VARCHAR(255),
	correo	VARCHAR(255),
	edad	INT,
	telefono	INT,
	mesero	BIT,
	chef	BIT,
	CRUD	BIT,
	PRIMARY KEY(idUsuario)
)
CREATE TABLE Cliente(
	idCliente	INT IDENTITY(1,1),
	nombre	VARCHAR(255),
	noTarjeta	NUMERIC(16),
	pin	INT,
	PRIMARY KEY(idCliente)
)
CREATE TABLE Queja(
	idQueja	INT IDENTITY(1,1),
	descripcion	TEXT,
	fecha	DATE,
	idCliente	INT,
	PRIMARY KEY(idQueja),
	FOREIGN KEY(idCliente) REFERENCES Cliente(idCliente)
)
CREATE TABLE Estado(
	idEstado	INT IDENTITY(1,1),
	descripcion	VARCHAR(255),
	PRIMARY KEY(idEstado)
)
CREATE TABLE Pedido(
	idPedido	INT IDENTITY(1,1),
	idPlatillo 	INT NOT NULL,
	cantidadPlatillos	INT,
	idBebida	INT NOT NULL,
	cantidadBebidas INT,
	PRIMARY KEY(idPedido),
	FOREIGN KEY(idPlatillo)	REFERENCES Platillo(idPlatillo),
	FOREIGN KEY(idBebida)	REFERENCES Bebida(idBebida)
)
CREATE TABLE Orden(
	idOrden INT IDENTITY(1,1),
	idCliente INT,
	idUsuario INT,
	idEstado	INT,
	fecha	VARCHAR(255),
	PRIMARY KEY(idOrden),
	FOREIGN KEY(idCliente) REFERENCES Cliente(idCliente),
	FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario),
	FOREIGN KEY(idEstado) REFERENCES Estado(idEstado)
)
CREATE TABLE DetalleOrden(
	idOrden	INT NOT NULL,
	idPedido INT NOT NULL,
	PRIMARY KEY(idOrden, idPedido),
	FOREIGN KEY(idOrden) REFERENCES Orden(idOrden),
	FOREIGN KEY(idPedido) REFERENCES Pedido(idPedido)
)
CREATE TABLE Factura(
	idFactura INT IDENTITY(1,1),
	idOrden INT NOT NULL,
	total	INT,
	PRIMARY KEY(idFactura),	
	FOREIGN KEY(idOrden) REFERENCES Orden(idOrden)
)

--INGRESO
--TipoDePlatillo
INSERT INTO TipoDePlatillo(nombre) VALUES('Postre')
INSERT INTO TipoDePlatillo(nombre) VALUES('Sopa')
INSERT INTO TipoDePlatillo(nombre) VALUES('Ensalada')
INSERT INTO TipoDePlatillo(nombre) VALUES('Desayunos')
INSERT INTO TipoDePlatillo(nombre) VALUES('Almuerzos')
INSERT INTO TipoDePlatillo(nombre) VALUES('Cenas')

--TipoDeBebida
INSERT INTO TipoDeBebida(nombre) VALUES('Jugo')
INSERT INTO TipoDeBebida(nombre) VALUES('Gaseosa')
INSERT INTO TipoDeBebida(nombre) VALUES('Te frio')

--Platillo
INSERT INTO Platillo(nombre, idTipoPlatillo, precio) VALUES('Pollo Asado', 5, 200)
INSERT INTO Platillo(nombre, idTipoPlatillo, precio) VALUES('Helado', 5, 25)
INSERT INTO Platillo(nombre, idTipoPlatillo, precio) VALUES('Huevo con frijoles', 4, 100)
INSERT INTO Platillo(nombre, idTipoPlatillo, precio) VALUES('Hamburguesa', 6, 75)
INSERT INTO Platillo(nombre, idTipoPlatillo, precio) VALUES('Ensalada de lechuga', 3, 20)

--Ingrediente
INSERT INTO Ingrediente(nombre) VALUES('Tomate')
INSERT INTO Ingrediente(nombre) VALUES('Pollo')
INSERT INTO Ingrediente(nombre) VALUES('Huevos')
INSERT INTO Ingrediente(nombre) VALUES('Frijol')
INSERT INTO Ingrediente(nombre) VALUES('Lechuga')
INSERT INTO Ingrediente(nombre) VALUES('Carne de cerdo')

--DetallePlatillo
INSERT INTO DetallePlatillo(idPlatillo, idIngrediente, cantidad) VALUES(3, 3, 10)
INSERT INTO DetallePlatillo(idPlatillo, idIngrediente, cantidad) VALUES(3, 4, 2)
INSERT INTO DetallePlatillo(idPlatillo, idIngrediente, cantidad) VALUES(5, 5, 11)
INSERT INTO DetallePlatillo(idPlatillo, idIngrediente, cantidad) VALUES(5, 1, 10)
INSERT INTO DetallePlatillo(idPlatillo, idIngrediente, cantidad) VALUES(4, 6, 2)

--Bebida
INSERT INTO Bebida(nombre, idTipoBebida, precio) VALUES('Pepsi', 2, 25)
INSERT INTO Bebida(nombre, idTipoBebida, precio) VALUES('Te Lipton', 3, 30)
INSERT INTO Bebida(nombre, idTipoBebida, precio) VALUES('Coca Cola', 2, 33)
INSERT INTO Bebida(nombre, idTipoBebida, precio) VALUES('Jugo de melocoton', 1, 23)

--Usuario
INSERT INTO Usuario(nombre, nick, contrasenia, correo, edad, telefono, mesero, chef, CRUD) VALUES('Manuel', 'mm', 'mm', 'm@gmail.com', 21, 24751254, 1, 0, 0)
INSERT INTO Usuario(nombre, nick, contrasenia, correo, edad, telefono, mesero, chef, CRUD) VALUES('Hermes', 'hr', 'hr', 'hr@gmail.com', 25, 24587754, 0, 1, 0)
INSERT INTO Usuario(nombre, nick, contrasenia, correo, edad, telefono, mesero, chef, CRUD) VALUES('Juan', 'jop', 'jop', 'jp@yahoo.com', 25, 21104244, 0, 0, 1)
INSERT INTO Usuario(nombre, nick, contrasenia, correo, edad, telefono, mesero, chef, CRUD) VALUES('Miguel', 'noj', 'noj', 'mnoj@hotmail.com', 26, 28682854, 1, 1, 1)

--Cliente
INSERT INTO Cliente(nombre, noTarjeta, pin) VALUES('Pedro', 2514254785412541, 2512)
INSERT INTO Cliente(nombre, noTarjeta, pin) VALUES('Ramon', 1254251247853698, 2123)
INSERT INTO Cliente(nombre, noTarjeta, pin) VALUES('cf', 2542542154875214, 1254)

--Queja
INSERT INTO Queja(descripcion, fecha, idCliente) VALUES('Escriba aqui sus quejas', '27-05-2014', 1)

--Estado
INSERT INTO Estado(descripcion) VALUES('En espera')
INSERT INTO Estado(descripcion) VALUES('Entregada')
INSERT INTO Estado(descripcion) VALUES('Cancelada')
INSERT INTO Estado(descripcion) VALUES('Pagada')

--Pedido
INSERT INTO Pedido(idPlatillo, cantidadPlatillos, idBebida, cantidadBebidas) VALUES(1, 1, 2, 2)
INSERT INTO Pedido(idPlatillo, cantidadPlatillos, idBebida, cantidadBebidas) VALUES(2, 2, 3, 1)
INSERT INTO Pedido(idPlatillo, cantidadPlatillos, idBebida, cantidadBebidas) VALUES(1, 3, 2, 2)

--Orden
INSERT INTO Orden(idCliente, idUsuario, idEstado, fecha) VALUES(2, 1, 1, '13-06-2014')
INSERT INTO Orden(idCliente, idUsuario, idEstado, fecha) VALUES(1, 1, 1, '13-06-2014')
INSERT INTO Orden(idCliente, idUsuario, idEstado, fecha) VALUES(3, 1, 1, '13-06-2014')

--DetalleOrden
INSERT INTO DetalleOrden(idOrden, idPedido) VALUES(1, 1)
INSERT INTO DetalleOrden(idOrden, idPedido) VALUES(1, 3)
INSERT INTO DetalleOrden(idOrden, idPedido) VALUES(2, 2)

--Factura
INSERT INTO Factura(idOrden, total) VALUES(2, 250)

--PROCESOS ALMACENADOS
GO
CREATE PROCEDURE agregarOrden @platillo VARCHAR(255), @bebida VARCHAR(255), @cantidadPlatillos INT, @cantidadBebidas INT, @cliente VARCHAR(255), @usuario VARCHAR(255), @fecha VARCHAR(255)
AS
BEGIN
	DECLARE @idPedido INT, @idPlatillo INT, @idBebida INT 
	SET @idPlatillo = (SELECT idPlatillo FROM Platillo WHERE nombre = @platillo)
	SET @idBebida = (SELECT idBebida FROM Bebida WHERE nombre = @bebida)
	SET @idPedido = (SELECT idPedido FROM Pedido WHERE cantidadPlatillos = @cantidadPlatillos AND cantidadBebidas = @cantidadBebidas AND idPlatillo = @idPlatillo AND idBebida = @idBebida)
	IF(@idPedido = null)
	BEGIN
		DECLARE @idCliente INT, @idUsuario INT
		SET @idCliente = (SELECT idCliente FROM Cliente WHERE nombre = @cliente)
		SET @idUsuario = (SELECT idUsuario FROM Usuario WHERE nombre = @usuario )
		INSERT INTO Pedido(idPlatillo, cantidadPlatillos, idBebida, cantidadBebidas) VALUES(@idPlatillo, @cantidadPlatillos, @idBebida, @cantidadBebidas)
		SET @idPedido = (SELECT idPedido FROM Pedido WHERE cantidadPlatillos = @cantidadPlatillos AND cantidadBebidas = @cantidadBebidas AND idPlatillo = @idPlatillo AND idBebida = @idBebida)
	END
	INSERT INTO Orden(idCliente, idUsuario, idEstado, fecha) VALUES(@idCliente, @idUsuario, 1, @idPedido)
END

EXEC 'Pollo Asado', 'Jugo', 2,2, 'Ramon', 'Manuel', '13-06-2014'


GO
CREATE TRIGGER actualizarTotal
AFTER UPDATE
	