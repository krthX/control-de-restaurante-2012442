package org.bryampaniagua.manejadores;

import org.bryampaniagua.database.Conexion;
import org.bryampaniagua.beans.Platillo;
import org.bryampaniagua.beans.Bebida;
import org.bryampaniagua.utilidades.Tabla;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ManejadorMenu{
	private Conexion cnx;
	private ResultSet rsResultado;
	private ObservableList<Platillo> listaDePlatillos;
	private ObservableList<Bebida> listaDeBebidas;
	private Tabla<Platillo> tablaPlatillo;
	private Tabla<Bebida> tablaBebida;

	public ManejadorMenu(Conexion cnx){
		this.cnx = cnx;
		this.listaDePlatillos = FXCollections.observableArrayList();
		this.listaDeBebidas = FXCollections.observableArrayList();
	}
	public void actualizarListaDePlatillos(){
		ResultSet rsPlatillos = cnx.execConsulta("SELECT Platillo.idPlatillo AS idPlatillo, Platillo.nombre AS Nombre, TipoDePlatillo.idTipoPlatillo AS idTipo, TipoDePlatillo.nombre AS Tipo, Platillo.precio AS Precio FROM Platillo INNER JOIN TipoDePlatillo ON Platillo.idTipoPlatillo = TipoDePlatillo.idTipoPlatillo");
		try{
			if(rsPlatillos != null){
				while(rsPlatillos.next()){
					Platillo platillo = new Platillo(rsPlatillos.getInt("idPlatillo"), rsPlatillos.getString("Nombre"), rsPlatillos.getInt("idTipo"), rsPlatillos.getString("Tipo"), rsPlatillos.getInt("Precio"));
					listaDePlatillos.add(platillo);
				}
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}
	public ObservableList<Platillo> getListaDePlatillos(){
		actualizarListaDePlatillos();
		return this.listaDePlatillos;
	}
	public void addPlatillo(Platillo platillo){
		cnx.execSentencia("INSERT INTO Platillo(nombre, idTipoPlatillo, precio) VALUES('"+platillo.getNombre()+"', "+platillo.getIdTipoPlatillo()+", "+platillo.getPrecio()+")");
		actualizarListaDePlatillos();
	}
	public void removePlatillo(Platillo platillo){
		cnx.execSentencia("DELETE FROM Platillo WHERE idPlatillo = "+platillo.getIdPlatillo()+"");
		actualizarListaDePlatillos();
	}
	public void createTablePlat(){
		if(tablaPlatillo == null){
			tablaPlatillo = new Tabla<Platillo>();
			String tipos[] = "String,String,Integer".split(",");
			String columnas[] = "Nombre,Tipo,Precio".split(",");
			String variables[] = "nombre,tipo,precio".split(",");
			tablaPlatillo.createTable(tipos, columnas, variables);
		}
	}
	public TableView<Platillo> addItemsTablePlat(){
		createTablePlat();
		tablaPlatillo.addItems(getListaDePlatillos());
		return tablaPlatillo.getTable();
	}
	public Platillo getSelectedPlat(){
		return tablaPlatillo.getSelectedIt();
	}
	
	public void actualizarListaDeBebidas(){
		ResultSet rsBebidas = cnx.execConsulta("SELECT Bebida.idBebida AS idBebida, Bebida.nombre AS Nombre, TipoDeBebida.idTipoBebida AS idTipo, TipoDeBebida.nombre AS Tipo, Bebida.precio AS Precio FROM Bebida INNER JOIN TipoDeBebida ON Bebida.idTipoBebida = TipoDeBebida.idTipoBebida");
		try{
			if(rsBebidas != null){
				while(rsBebidas.next()){
					Bebida bebida = new Bebida(rsBebidas.getInt("idBebida"), rsBebidas.getString("Nombre"), rsBebidas.getInt("idTipo"), rsBebidas.getString("Tipo"), rsBebidas.getInt("Precio"));
					listaDeBebidas.add(bebida);
				}
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}
	public ObservableList<Bebida> getListaDeBebidas(){
		actualizarListaDeBebidas();
		return this.listaDeBebidas;
	}
	public void addBebida(Bebida bebida){
		cnx.execSentencia("INSERT INTO Bebida(nombre, idTipoPlatillo, precio) VALUES('"+bebida.getNombre()+"', "+bebida.getIdTipo()+", "+bebida.getPrecio()+")");
		actualizarListaDeBebidas();
	}
	public void removeBebida(Bebida bebida){
		cnx.execSentencia("DELETE FROM Bebida WHERE idBebida = "+bebida.getIdBebida()+"");
		actualizarListaDeBebidas();
	}
	public void createTableBeb(){
		if(tablaBebida == null){
			tablaBebida = new Tabla<Bebida>();
			String tipos[] = "String,String,Integer".split(",");
			String columnas[] = "Nombre,Tipo,Precio".split(",");
			String variables[] = "nombre,tipo,precio".split(",");
			tablaBebida.createTable(tipos, columnas, variables);
		}
	}
	public TableView<Bebida> addItemsTableBeb(){
		createTableBeb();
		tablaBebida.addItems(getListaDeBebidas());
		return tablaBebida.getTable();
	}
	public Bebida getSelectedBeb(){
		return tablaBebida.getSelectedIt();
	}
}