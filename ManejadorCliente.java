package org.bryampaniagua.manejadores;

import org.bryampaniagua.database.Conexion;
import org.bryampaniagua.beans.Cliente;
import org.bryampaniagua.utilidades.Tabla;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ManejadorCliente{
	private Conexion cnx;
	private ObservableList<Cliente> listaDeClientes;
	private Tabla<Cliente> tablaCliente;

	public ManejadorCliente(Conexion cnx){
		this.cnx = cnx;
		this.listaDeClientes = FXCollections.observableArrayList();
	}
	public void actualizarListaDeClientes(){
		ResultSet rsClientes = cnx.execConsulta("SELECT idCliente, nombre, noTarjeta, pin FROM Cliente");
		try{
			if(rsClientes != null){
				while(rsClientes.next()){
					Cliente cliente = new Cliente(rsClientes.getInt("idCliente"), rsClientes.getString("nombre"), rsClientes.getLong("noTarjeta"), rsClientes.getInt("pin"));
					listaDeClientes.add(cliente);
				}
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}
	public ObservableList<Cliente> getListaDeClientes(){
		actualizarListaDeClientes();
		return listaDeClientes;
	}
	public void addCliente(Cliente cliente){
		cnx.execSentencia("INSERT INTO Cliente(nombre, noTarjeta, pin) VALUES('"+cliente.getNombre()+"', "+cliente.getNoTarjeta()+", "+cliente.getPin()+"");
		actualizarListaDeClientes();
	}
	public void createTableC(){
		if(tablaCliente == null){
			tablaCliente = new Tabla<Cliente>();
			String tipos[] = "String,Long,Integer".split(",");
			String columnas[] = "Nombre,Tarjeta,Pin".split(",");
			String variables[] = "nombre,noTarjeta,pin".split(",");
			tablaCliente.createTable(tipos, columnas, variables);
		}
	}
	public TableView<Cliente> addItemsTable(){
		createTableC();
		tablaCliente.addItems(getListaDeClientes());
		return tablaCliente.getTable();
	}
	public Cliente getSelected(){
		return tablaCliente.getSelectedIt();
	}
}