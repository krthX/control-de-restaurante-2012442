package org.bryampaniagua.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class Platillo{
	private IntegerProperty idPlatillo, idTipoPlatillo, precio;
	private StringProperty nombre, tipo;
	
	public Platillo(){
		this.idPlatillo = new SimpleIntegerProperty();
		this.idTipoPlatillo = new SimpleIntegerProperty();
		this.precio = new SimpleIntegerProperty();
		this.nombre = new SimpleStringProperty();
		this.tipo = new SimpleStringProperty();
	}
	public Platillo(int idPlatillo, String nombre, int idTipoPlatillo, String tipo, int precio){
		this.idPlatillo = new SimpleIntegerProperty(idPlatillo);
		this.idTipoPlatillo = new SimpleIntegerProperty(idTipoPlatillo);
		this.precio = new SimpleIntegerProperty(precio);
		this.nombre = new SimpleStringProperty(nombre);
		this.tipo = new SimpleStringProperty(tipo);
	}
	
	public int getIdPlatillo(){
		return idPlatillo.get();
	}
	public void setIdPlatillo(int idPlatillo){
		this.idPlatillo.set(idPlatillo);
	}
	public IntegerProperty idPlatilloProperty(){
		return idPlatillo;
	}
	
	public String getNombre(){
		return nombre.get();
	}
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	public StringProperty nombreProperty(){
		return nombre;
	}
	
	public int getIdTipoPlatillo(){
		return idTipoPlatillo.get();
	}
	public void setIdTipoPlatillo(int idTipoPlatillo){
		this.idTipoPlatillo.set(idTipoPlatillo);
	}
	public IntegerProperty idTipoPlatillo(){
		return idTipoPlatillo;
	}
	
	public String getTipo(){
		return tipo.get();
	}
	public void setTipo(String tipo){
		this.tipo.set(tipo);
	}
	public StringProperty tipoProperty(){
		return tipo;
	}
	
	public int getPrecio(){
		return precio.get();
	}
	public void setPrecio(int precio){
		this.precio.set(precio);
	}
	public IntegerProperty precioProperty(){
		return precio;
	}
}