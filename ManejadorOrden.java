package org.bryampaniagua.manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import org.bryampaniagua.database.Conexion;
import org.bryampaniagua.beans.Pedido;
import org.bryampaniagua.beans.Orden;
import org.bryampaniagua.utilidades.Tabla;

public class ManejadorOrden{
	private Conexion cnx;
	private ObservableList<Orden> listaDeOrdenes;
	private ObservableList<Pedido> listaDePedidos;
	private int idOrden;
	private Tabla<Orden> tablaOrden;
	private Tabla<Pedido> tablaPedido;

	public ManejadorOrden(Conexion cnx){
		this.cnx = cnx;
		this.listaDeOrdenes = FXCollections.observableArrayList();
		this.listaDePedidos = FXCollections.observableArrayList();
	}
	public void actualizarListaDeOrdenes(){
		listaDeOrdenes.clear();
		ResultSet rsOrden = cnx.execConsulta("SELECT DISTINCT Orden.idOrden AS idOrden, Cliente.nombre AS Cliente, Cliente.idCliente AS idCliente, Usuario.nombre AS Mesero ,Usuario.idUsuario AS idUsuario, Estado.descripcion AS Estado, Estado.idEstado AS idEstado, Orden.fecha AS Fecha FROM DetalleOrden INNER JOIN Orden ON DetalleOrden.idOrden = Orden.idOrden	INNER JOIN Cliente ON Orden.idCliente = Cliente.idCliente	INNER JOIN Usuario ON Orden.idUsuario = Usuario.idUsuario	INNER JOIN Estado ON Orden.idEstado = Estado.idEstado	ORDER BY Orden.idOrden");
		try{
			if(rsOrden != null){
				while(rsOrden.next()){
					Orden orden = new Orden(rsOrden.getInt("idOrden"),rsOrden.getString("Cliente"), rsOrden.getInt("idCliente"), rsOrden.getString("Mesero"), rsOrden.getInt("idUsuario"), rsOrden.getString("Estado"), rsOrden.getInt("idEstado"), rsOrden.getString("Fecha"));
					listaDeOrdenes.add(orden);
				}
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
	public ObservableList<Orden> getListaDeOrdenes(){
		actualizarListaDeOrdenes();
		return listaDeOrdenes;
	}
	public void eliminarOrden(Orden orden){
		cnx.execConsulta("DELETE FROM DetalleOrden WHERE idOrden = "+orden.getIdOrden());
		actualizarListaDeOrdenes();
	}
	public void createTableO(){
		if(tablaOrden == null){
			tablaOrden = new Tabla<Orden>();
			String tipos[] = "String,String,String,String".split(",");
			String columnas[] = "Cliente,Mesero,Estado,Fecha".split(",");
			String variables[] = "cliente,usuario,estado,fecha".split(",");
			tablaOrden.createTable(tipos, columnas, variables);
		}
	}
	public TableView<Orden> addItemsTable(){
		createTableO();
		tablaOrden.addItems(getListaDeOrdenes());
		return tablaOrden.getTable();
	}
	public Orden getSelected(){
		return tablaOrden.getSelectedIt();
	}
	
	
	public void actualizarListaDePedidos(int idOrden){
		listaDePedidos.clear();
		ResultSet rsPedido = cnx.execConsulta("SELECT Pedido.idPedido AS idPedido, Platillo.nombre AS Platillo, cantidadPlatillos, Bebida.nombre AS Bebida, cantidadBebidas FROM DetalleOrden  INNER JOIN Pedido ON DetalleOrden.idPedido = Pedido.idPedido  INNER JOIN Platillo ON Pedido.idPlatillo = Platillo.idPlatillo  INNER JOIN Bebida ON Pedido.idBebida = Bebida.idBebida WHERE idOrden = "+idOrden+"");
		try{
			while(rsPedido.next()){
				Pedido pedido = new Pedido(rsPedido.getInt("idPedido"), rsPedido.getString("Platillo"), rsPedido.getInt("cantidadPlatillos"), rsPedido.getString("Bebida"), rsPedido.getInt("cantidadBebidas"));
				listaDePedidos.add(pedido);
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}
	public ObservableList<Pedido> getListaDePedidos(int idOrden){
		actualizarListaDePedidos(idOrden);
		return this.listaDePedidos;
	}
	public void createTableP(){
		if(tablaPedido == null){
			tablaPedido = new Tabla<Pedido>();
			String types[] = "String,Integer,String,Integer".split(",");			
			String columns[] = "Platillo,cantidadPlatillo,Bebida,cantidadBebida".split(",");
			String vars[] = "platillo,cantidadPlatillos,bebida,cantidadBebidas".split(",");
			tablaPedido.createTable(types, columns, vars);
		}
	}
	public TableView<Pedido> addItemsTableP(int idOrden){
		createTableP();
		tablaPedido.addItems(getListaDePedidos(idOrden));
		return tablaPedido.getTable();
	}
	public Pedido getSelectedPed(){
		return tablaPedido.getSelectedIt();
	}
}