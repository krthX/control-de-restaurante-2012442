package org.bryampaniagua.sistema;

import javafx.application.Application;

import org.bryampaniagua.sistema.Login;
	/**
		@author BryamPaniagua
		
		Contiene el metodo principal, que unicamente ejecuta un metodo de 
			javafx.application.Application
				llamado launch();
		que recibe el bytecode de la clase App que extiende de la clase Application
		y  los argumentos iniciales	que le pasemos al ejecutar en consola.
	**/
public class Principal{
	/**
		Ejecuta el metodo launch();
	**/
	public static void main(String[] args){
		Application.launch(Login.class, args);
	}
}