package org.bryampaniagua.database;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.Statement;

	/**
		
		@author BryamPaniagua
		
		Esta clase carga los drivers y realiza la conexion con la 
		base de datos en SQL.
		Realiza las consultas y sentencias que recibe como parametros
		sus metodos.
	**/
public class Conexion{
	public Connection cnx;
	private Statement estado;
	
	/**
		
		En el se carga el driver, se conecta la base de datos y se crea 
		un objeto con dicha conexion que servira despues
	**/
	public Conexion(){
		try{		
			System.out.println("Cargando conexion...");
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			System.out.println("Cargando driver...");
			cnx = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=dbRestaurante2012440", "BP", "123");
			System.out.println("Cargando base de datos...");
			estado = cnx.createStatement();
			System.out.println("Creando estado..");
		}catch(ClassNotFoundException cnfe){
			cnfe.printStackTrace();
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}	
	}
	/**
		
		Ejecuta la sentencia que recibe como parametro
		@return resultado
	**/
	public boolean execSentencia(String sentencia){
		boolean resultado = false;
		try{
			resultado = estado.execute(sentencia);
		}catch(SQLException sqle){
			sqle.printStackTrace();
			return resultado;
		}
		return resultado;
	}
	/**
		
		Ejecuta la consulta que se le envia como parametro
		@return rsResultado
	**/
	public ResultSet execConsulta(String consulta){
		ResultSet rsResultado = null;
		try{
			rsResultado = estado.executeQuery(consulta);
		}catch(SQLException sqle){
			sqle.printStackTrace();
			return rsResultado;
		}
		return rsResultado;
	}
}