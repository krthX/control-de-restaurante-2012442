package org.bryampaniagua.sistema;

import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TableView;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.geometry.Pos;
import javafx.geometry.Insets;

import org.bryampaniagua.sistema.Login;
import org.bryampaniagua.beans.Orden;
import org.bryampaniagua.beans.Usuario;
import org.bryampaniagua.beans.Pedido;
import org.bryampaniagua.beans.Cliente;
import org.bryampaniagua.beans.Platillo;
import org.bryampaniagua.beans.Bebida;
import org.bryampaniagua.manejadores.ManejadorUsuario;
import org.bryampaniagua.manejadores.ManejadorMenu;
import org.bryampaniagua.manejadores.ManejadorCliente;
import org.bryampaniagua.manejadores.ManejadorOrden;

public class App implements EventHandler<Event>{
	private Tab tbPrincipal, tbAddUser,tbChangeP, tbAddP, tbPedidos, tbChangeUser, tbSeeP, tbUsuarios, tbMenu, tbOrdenes, tbAddO;
	private Tab tbPlatillos, tbBebidas;
	private ToolBar tlbCRUD, tlbConf, tlbOrdenes;
	private Button btnSalir, btnMenu, btnOrdenes, btnUsuarios, btnPlatillos, btnBebidas, btnSeeP, btnAddP, btnChangeP, btnDeleteP;
	private Button btnSeeO, btnAddO, btnCancelO, btnChangeO;
	private Label lbBienvenida;
	private BorderPane bpListP, bpPrincipal, bpListO, bpListUs, bpListPlat, bpListBeb;
	private TableView<Orden> tvListaOrdenes;
	private TableView<Pedido> tvListaPedidos;
	private TableView<Bebida> tvListaBebidas;
	private TableView<Platillo> tvListaPlatillos;
	private TableView<Usuario> tvListaUsuarios;
	private TableView<Cliente> tvListaClientes;
	private GridPane gpSeeP, gpPedido, gpUsers, gpContentList, gpConfig;
	private TextField tfNombre,tfNick,tfCorreo, tfEdad, tfTelefono, pfContrasenia;
	private TextField tfPlatilloP, tfCantidadPlatP, tfBebidaP, tfCantidadBebP;
	private ManejadorUsuario manus;
	private ManejadorOrden manord;
	private ManejadorMenu manme;
	private ManejadorCliente mancli;
	private Login login;
	
	public App(Login login){
		this.login = login;
		manus = login.getManus();
		manord = new ManejadorOrden(login.getCnx());
		manme = new ManejadorMenu(login.getCnx());
		mancli = new ManejadorCliente(login.getCnx());
	}
	/**
		
		Se mostrara cuando el usuario mesero inicie sesion pues
		es su tab inicial de aqui partira a pedidos y al CRUD de 
		pedidos
			@tbPrincipal
	**/
	public Tab getTabPrincipal(){
		if(tbPrincipal == null){
			tbPrincipal = new Tab("Principal");
			tbPrincipal.setClosable(false);
			
			tbPrincipal.setContent(this.getContenedorPrincipal());
		}
		return tbPrincipal;
	}
	public Tab tabOrdenes(){
		if(tbOrdenes == null){
			tbOrdenes = new Tab("Ordenes");
			tbOrdenes.setClosable(false);
			
			tbOrdenes.setContent(this.getContentListO());
		}
		return tbOrdenes;
	}
	public Tab getTabAddO(){
		if(tbAddO == null){
			tbAddO = new Tab("Add Orden");
		}
		return tbAddO;
	}
	/**
	
		Cuando el mesero desee realizar acciones con relacion a 
		ordenes se mostrara esta tab
	**/
	public Tab tabPedidos(int orden){
		if(tbPedidos == null){
			tbPedidos = new Tab("Pedidos");	
		}
		tbPedidos.setContent(this.getContentListP(orden));
		return tbPedidos;
	}
	/**
		
		
		Se mostrara cuando el modulo mesero desee agregar una orden
		recibe el contenido del metodo getContenedorAgregarPedido
		@return tbAgregarPedido
	**/
	public Tab tabAddP(){
		if(tbAddP == null){
			tbAddP = new Tab("Agregar Pedido");
			tbAddP.setClosable(false);
			
			tbAddP.setContent(this.getContentPedido(new Pedido()));
		}
		return tbAddP;
	}	
		/**
		
		Es parte del CRUD de pedidos en este caso se mostrara 
		cuando se desee modificar un pedido existente
		@return tbChangeP
	**/
	public Tab tabChangeP(Pedido pedido){
		if(tbChangeP == null){
			tbChangeP = new Tab("Modificar Pedidos");
				
			tbChangeP.setContent(this.getContentPedido(pedido));
		}
		return tbChangeP;
	}
	/**
	
		Cuando el mesero desee ver una orden existente se enviara 
		a este tab
		@return tbVerP
	**/
	public Tab tabSeeP(Pedido pedido){
		if(tbSeeP == null){
			tbSeeP = new Tab("Ver Pedidos");
				
		}
			tbSeeP.setContent(this.getContentSeeP(pedido));
		return tbSeeP;
	}
	public Tab tabUsuarios(){
		if(tbUsuarios == null){
			tbUsuarios = new Tab("Usuarios");
				
			tbUsuarios.setContent(this.getContentUsuarios());
		}
		return tbUsuarios;
	}
	public Tab tabBebidas(){
		if(tbBebidas == null){
			tbBebidas = new Tab("Bebidas");
				
			tbBebidas.setContent(this.getContentBebidas());
		}
		return tbBebidas;
	}
	public Tab tabPlatillos(){
		if(tbPlatillos == null){
			tbPlatillos = new Tab("Platillos");
				
			tbPlatillos.setContent(this.getContentPlatillos());
		}
		return tbPlatillos;
	}
	/**
		
		Recibe el contenido para la edicion del perfil en cualquier modulo
		ejecutando el metodo getContenedorEditarPerfil() y retorna el tab
		@return tbEditarPerfil
	**/
	public Tab tabChangeUser(Usuario user){
		if(tbChangeUser == null){
			tbChangeUser = new Tab("Modificar Usuario");
			tbChangeUser.setClosable(false);
			
			tbChangeUser.setContent(this.getContentUs(user));
		}
		return tbChangeUser;
	}
	/**
		
		Recibe el contenido para la edicion del perfil en cualquier modulo
		ejecutando el metodo getContenedorEditarPerfil() y retorna el tab
		@return tbEditarPerfil
	**/
	public Tab tabAddUser(){
		if(tbAddUser == null){
			tbAddUser = new Tab("Agregar Usuario");
			tbAddUser.setClosable(false);
			
			tbAddUser.setContent(this.getContentUs(new Usuario()));
		}
		return tbAddUser;
	}
	public Tab tabMenu(){
		if(tbMenu == null){
			tbMenu = new Tab("Menu");
				
			// tbMenu.setContent(this.getContentSeeP(pedido));
		}
		return tbMenu;
	}
	
	/**
	
		Contenido para la tab de un usuario en el modulo
		mesero
		@return bpPropiedadesMesero
	**/
	public BorderPane getContenedorPrincipal(){
		if(bpPrincipal == null){
			bpPrincipal = new BorderPane();
			
			bpPrincipal.setLeft(this.contentConfiguracion());
		}
		return bpPrincipal;
	}	
	/**
		
		Agregara la lista de pedidos y la barra de herramientas del CRUD pedido
		al borderPane
		@return bpListP
	**/
	public BorderPane getContentListP(int orden){
		if(bpListP == null){
			bpListP = new BorderPane();
			
			bpListP.setTop(this.getTlBCRUD());
		}
		bpListP.setCenter(this.getListaPedidos(orden));
		return bpListP;
	}	
	public BorderPane getContentUsuarios(){
		if(bpListUs == null){
			bpListUs = new BorderPane();
		}
		bpListUs.setCenter(this.getListaUsuarios());
		return bpListUs;
	}	
	public BorderPane getContentPlatillos(){
		if(bpListPlat == null){
			bpListPlat = new BorderPane();
		}
		bpListPlat.setCenter(this.getListaPlatillos());
		return bpListPlat;
	}	
	public BorderPane getContentBebidas(){
		if(bpListBeb == null){
			bpListBeb = new BorderPane();
		}
		bpListBeb.setCenter(this.getListaBebidas());
		return bpListBeb;
	}	
	public BorderPane getContentListO(){
		if(bpListO == null){
			bpListO = new BorderPane();
			
			bpListO.setTop(this.getTlbCrudO());
		}
		bpListO.setCenter(this.getListaOrdenes());
		return bpListO;
	}
	/**
		Contenido necesario para la agregacion de una orden
		@return gpPedido
	**/
	public GridPane getContentPedido(Pedido pedido){
		if(gpPedido == null){
			gpPedido = new GridPane();
			gpPedido.setAlignment(Pos.CENTER);
						
			tfPlatilloP = new TextField();
			tfPlatilloP.addEventHandler(KeyEvent.KEY_RELEASED, this);
			
			tfCantidadPlatP = new TextField();
			tfCantidadPlatP.addEventHandler(KeyEvent.KEY_RELEASED, this);
			
			tfBebidaP = new TextField();
			tfBebidaP.addEventHandler(KeyEvent.KEY_RELEASED, this);
			
			tfCantidadBebP = new TextField();
			tfCantidadBebP.addEventHandler(KeyEvent.KEY_RELEASED, this);
			
			gpPedido.add(new Label("Platillo "), 0,0);
			gpPedido.add(new Label("Cantidad Platillos "), 0,1);
			gpPedido.add(new Label("Bebida "), 0,2);
			gpPedido.add(new Label("Cantidad Bebidas "), 0,3);
		}
		tfPlatilloP.setText(pedido.getPlatillo());
		tfCantidadPlatP.setText(String.valueOf(pedido.getCantidadPlatillos()));
		tfBebidaP.setText(pedido.getBebida());
		tfCantidadBebP.setText(String.valueOf(pedido.getCantidadBebidas()));
		
		gpPedido.add(tfPlatilloP, 1,0);
		gpPedido.add(tfCantidadPlatP, 1,1);
		gpPedido.add(tfBebidaP, 1,2);
		gpPedido.add(tfCantidadBebP, 1,3);
		return gpPedido;
	}
	/**
		
		Contenido necesario para la visualizacion de una orden
		@return gpVerPedido
	**/
	public GridPane getContentSeeP(Pedido pedido){
		if(gpSeeP == null){
			gpSeeP = new GridPane();
			gpSeeP.setAlignment(Pos.CENTER);
			
			gpSeeP.add(new Label("Platillo"), 0,0);
			gpSeeP.add(new Label("Cantidad Platillos"), 0,1);
			gpSeeP.add(new Label("Bebida"), 0,2);
			gpSeeP.add(new Label("Cantidad Bebidas"), 0,3);
		}
		gpSeeP.add(new Label(String.valueOf(pedido.getPlatillo())), 1,0);
		gpSeeP.add(new Label(String.valueOf(pedido.getCantidadPlatillos())), 1,1);
		gpSeeP.add(new Label(String.valueOf(pedido.getBebida())), 1,2);
		gpSeeP.add(new Label(String.valueOf(pedido.getCantidadBebidas())), 1,3);
		
		return gpSeeP;
	}
	
	/**
		
		Controles para la edicion del perfil de un usuario
		@return bpEditarPerfil
	**/
	public GridPane getContentUs(Usuario user){
		if(gpUsers == null){
			gpUsers = new GridPane();
			gpUsers.setAlignment(Pos.CENTER);
			
			tfNombre = new TextField();
			tfNombre.addEventHandler(KeyEvent.KEY_RELEASED, this);			
			
			tfNick = new TextField();
			tfNick.addEventHandler(KeyEvent.KEY_RELEASED, this);			
			
			tfCorreo = new TextField();
			tfCorreo.addEventHandler(KeyEvent.KEY_RELEASED, this);
			
			tfEdad = new TextField();
			tfEdad.addEventHandler(KeyEvent.KEY_RELEASED, this);			
			
			tfTelefono = new TextField();
			tfTelefono.addEventHandler(KeyEvent.KEY_RELEASED, this);
			
			pfContrasenia = new PasswordField();
			pfContrasenia.addEventHandler(KeyEvent.KEY_RELEASED, this);
				
			gpUsers.add(new Label("Nombre"), 0,0);
			gpUsers.add(new Label("Nick"), 0,1);
			gpUsers.add(new Label("Correo"), 0,2);
			gpUsers.add(new Label("Edad"), 0,3);
			gpUsers.add(new Label("Telefono"), 0,4);
			gpUsers.add(new Label("Contrasenia"), 0,5);
			
		}	
		tfNombre.setText(user.getNombre());
		tfNick.setText(user.getNick());
		tfCorreo.setText(user.getCorreo());
		tfEdad.setText(String.valueOf(user.getEdad()));
		tfTelefono.setText(String.valueOf(user.getTelefono()));
		pfContrasenia.setText(user.getContrasenia());
			
		gpUsers.add(tfNombre, 1,0);
		gpUsers.add(tfNick, 1,1);
		gpUsers.add(tfCorreo, 1,2);
		gpUsers.add(tfEdad, 1,3);
		gpUsers.add(tfTelefono, 1,4);
		gpUsers.add(pfContrasenia, 1,5);
		
		return gpUsers;
	}
	
	/**
		Contenido para acciones principales en cada modulo
		@return tlbConf
	**/
	public ToolBar contentConfiguracion(){		
		lbBienvenida = new Label("BIENVENIDO, has ingresado como "+manus.getUsuarioIngresado().getNombre());
		if(tlbConf == null){
			tlbConf = new ToolBar();
			
			btnSalir = new Button("SALIR");
			btnSalir.addEventHandler(ActionEvent.ACTION, this);
			
			btnMenu = new Button("Menu");
			btnMenu.addEventHandler(ActionEvent.ACTION, this);
		}
		tlbConf.getItems().addAll(lbBienvenida, btnMenu);
		if(manus.getUsuarioIngresado().getMesero() == 1 || manus.getUsuarioIngresado().getChef() == 1){
			btnOrdenes = new Button("Ordenes");
			btnOrdenes.addEventHandler(ActionEvent.ACTION, this);
			tlbConf.getItems().add(btnOrdenes);
		}
		if(manus.getUsuarioIngresado().getCRUD() == 1){
			btnUsuarios = new Button("Usuarios"); 
			btnUsuarios.addEventHandler(ActionEvent.ACTION, this);
			
			btnPlatillos = new Button("Platillos");
			btnPlatillos.addEventHandler(ActionEvent.ACTION, this);
			
			btnBebidas = new Button("Bebidas");
			btnBebidas.addEventHandler(ActionEvent.ACTION, this);
			tlbConf.getItems().addAll(btnUsuarios, btnPlatillos, btnBebidas);
		}
		tlbConf.getItems().add(btnSalir);
		
		return tlbConf;
	}
	public ToolBar getTlbCrudO(){		
		if(tlbOrdenes == null){
			tlbOrdenes = new ToolBar();
			
			btnSeeO = new Button("See"); 
			btnSeeO.addEventHandler(ActionEvent.ACTION, this);
			
			btnAddO = new Button("Add");
			btnAddO.addEventHandler(ActionEvent.ACTION, this);
			
			btnCancelO = new Button("Cancel");
			btnCancelO.addEventHandler(ActionEvent.ACTION, this);
			
			btnChangeO = new Button("Change"); 
			btnChangeO.addEventHandler(ActionEvent.ACTION, this);
			
			tlbOrdenes.getItems().addAll(btnSeeO, btnAddO, btnCancelO, btnChangeO);
		}		
		return tlbOrdenes;
	}
	
	/**
		
		Es la barra de herramientas que se mostrar en el CRUD de pedidos
		@return tlbCRUD
	**/
	public ToolBar getTlBCRUD(){
		if(tlbCRUD == null){
			tlbCRUD = new ToolBar();
			
			btnSeeP = new Button("Ver");
			btnSeeP.addEventHandler(ActionEvent.ACTION, this);
			
			btnAddP = new Button("Agregar");
			btnAddP.addEventHandler(ActionEvent.ACTION, this);
			
			btnChangeP = new Button("Modificar");
			btnChangeP.addEventHandler(ActionEvent.ACTION, this);
			
			btnDeleteP = new Button("Eliminar");
			btnDeleteP.addEventHandler(ActionEvent.ACTION, this);
			
			tlbCRUD.getItems().addAll(btnSeeP, btnAddP, btnChangeP, btnDeleteP);
		}
		return tlbCRUD;
	}
	/**
		Crea las columna de la tabla de pedidos e ingresa los valores
		a las columnas ejecutando el metodo que retorna la lista
		actualizada en el manejadorPedido
		@return tvListaPedidos
	**/
	public TableView<Orden> getListaOrdenes(){
		tvListaOrdenes = manord.addItemsTable();
		return tvListaOrdenes;
	}
	public TableView<Pedido> getListaPedidos(int orden){
		tvListaPedidos = manord.addItemsTableP(orden);
		return tvListaPedidos;
	}
	public TableView<Usuario> getListaUsuarios(){
		tvListaUsuarios = manus.addItemsTableUs();
		return tvListaUsuarios;
	}
	public TableView<Cliente> getListaClientes(){
		tvListaClientes = mancli.addItemsTable();
		return tvListaClientes;
	}
	public TableView<Platillo> getListaPlatillos(){
		tvListaPlatillos = manme.addItemsTablePlat();
		return tvListaPlatillos;
	}
	public TableView<Bebida> getListaBebidas(){
		tvListaBebidas = manme.addItemsTableBeb();
		return tvListaBebidas;
	}
	
	public void enviarTab(Tab tab, boolean limpiar){
		login.addTab(tab, limpiar);
	}
	
	public void handle(Event event){
		if(event instanceof ActionEvent){
			if(event.getSource().equals(btnOrdenes)){
				enviarTab(tabOrdenes(), false);
			}else if(event.getSource().equals(btnUsuarios)){
				enviarTab(tabUsuarios(), false);
			}else if(event.getSource().equals(btnPlatillos)){
				enviarTab(tabPlatillos(), false);
			}else if(event.getSource().equals(btnBebidas)){
				enviarTab(tabBebidas(), false);
			}else if(event.getSource().equals(btnMenu)){
				enviarTab(tabMenu(), false);
			}else if(event.getSource().equals(btnSalir)){
				manus.salirDeSesion();
			}else if(event.getSource().equals(btnSeeP)){
				Pedido ped = manord.getSelectedPed();
				enviarTab(tabSeeP(ped), false);
			}else if(event.getSource().equals(btnAddP)){
				//Hola
			}else if(event.getSource().equals(btnChangeP)){
				//Hola
			}else if(event.getSource().equals(btnDeleteP)){
				//Hola
			}else if(event.getSource().equals(btnSeeO)){
				Orden or = manord.getSelected();
				enviarTab(tabPedidos(or.getIdOrden()), false);
			}else if(event.getSource().equals(btnAddO)){
				//Hola
			}else if(event.getSource().equals(btnCancelO)){
				//Hola
			}else if(event.getSource().equals(btnChangeO)){
				//Hola
			}
		}else if(event instanceof KeyEvent){
			if(event.getSource().equals(tfNombre) || event.getSource().equals(tfNick) || event.getSource().equals(tfCorreo) || event.getSource().equals(tfEdad) || event.getSource().equals(tfTelefono) || event.getSource().equals(pfContrasenia)){
				if(!tfNombre.getText().equals("") && !tfNick.getText().equals("") && !tfCorreo.getText().equals("") && !tfEdad.getText().equals("") && !tfTelefono.getText().equals("") && !pfContrasenia.getText().equals("")){
					Usuario usuario = new Usuario(0,tfNombre.getText(), tfNick.getText(), pfContrasenia.getText(), tfCorreo.getText(), Integer.parseInt(tfEdad.getText()), Integer.parseInt(tfTelefono.getText()),0,0,0);
					manus.modificarUsuario(usuario);
					enviarTab(tabUsuarios(), false);
				}	
			}
		}
		if(manus.getUsuarioIngresado() == null){
			enviarTab(login.getTabLogin(), true);
		}
	}
}