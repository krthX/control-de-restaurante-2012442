package org.bryampaniagua.beans;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Bebida{
	private IntegerProperty idBebida, idTipo, precio;
	private StringProperty nombre, tipo;
	
	public Bebida(){
		idBebida = new SimpleIntegerProperty();
		nombre = new SimpleStringProperty();
		idTipo = new SimpleIntegerProperty();
		tipo = new SimpleStringProperty();
		precio = new SimpleIntegerProperty();
	}
	public Bebida(int idBebida, String nombre, int idTipo, String tipo, int precio){
		this.idBebida = new SimpleIntegerProperty(idBebida);
		this.nombre = new SimpleStringProperty(nombre);
		this.idTipo = new SimpleIntegerProperty(idTipo);
		this.tipo = new SimpleStringProperty(tipo);	
		this.precio = new SimpleIntegerProperty(precio);	
	}
	
	public int getIdBebida(){
		return idBebida.get();
	}
	public void setIdBebida(int idBebida){
		this.idBebida.set(idBebida);
	}
	public IntegerProperty idBebidaProperty(){
		return idBebida;
	}
	
	public String getNombre(){
		return nombre.get();
	}
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	public StringProperty nombreProperty(){
		return nombre;
	}
	
	public int getIdTipo(){
		return idTipo.get();
	}
	public void setIdTipo(int idTipo){
		this.idTipo.set(idTipo);
	}
	public IntegerProperty idTipoProperty(){
		return idTipo;
	}
	
	public String getTipo(){
		return tipo.get();
	}
	public void setTipo(String tipo){
		this.tipo.set(tipo);
	}
	public StringProperty tipoProperty(){
		return tipo;
	}
	
	public int getPrecio(){
		return precio.get();
	}
	public void setPrecio(int precio){
		this.precio.set(precio);
	}
	public IntegerProperty precioProperty(){
		return precio;
	}
}