package org.bryampaniagua.beans;

import org.bryampaniagua.beans.Pedido;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Orden{
	private StringProperty cliente, usuario, estado, fecha;
	private IntegerProperty idOrden, idEstado, idCliente, idUsuario;
	
	public Orden(){
		this.cliente = new SimpleStringProperty();
		this.idCliente = new SimpleIntegerProperty();
		this.usuario = new SimpleStringProperty();
		this.idUsuario = new SimpleIntegerProperty();
		this.estado = new SimpleStringProperty();
		this.idEstado = new SimpleIntegerProperty();
		this.fecha = new SimpleStringProperty();
		this.idOrden = new SimpleIntegerProperty();
	}
	public Orden(int idOrden, String cliente, int idCliente, String usuario, int idUsuario, String estado, int idEstado, String fecha){
		this.cliente = new SimpleStringProperty(cliente);
		this.idCliente = new SimpleIntegerProperty(idCliente);
		this.usuario = new SimpleStringProperty(usuario);
		this.idUsuario = new SimpleIntegerProperty(idUsuario);
		this.estado = new SimpleStringProperty(estado);
		this.idEstado = new SimpleIntegerProperty(idEstado);
		this.fecha = new SimpleStringProperty(fecha);
		this.idOrden = new SimpleIntegerProperty(idOrden);
	}
	
	public int getIdOrden(){
		return idOrden.get();
	}
	public void setIdOrden(int idOrden){
		this.idOrden.set(idOrden);
	}
	public IntegerProperty idOrdenProperty(){
		return idOrden;
	}
	
	public String getCliente(){
		return cliente.get();
	}
	public void setCliente(String cliente){
		this.cliente.set(cliente);
	}
	public StringProperty clienteProperty(){
		return cliente;
	}
	
	public int getIdCliente(){
		return idCliente.get();
	}
	public void setIdCliente(int idCliente){
		this.idCliente.set(idCliente);
	}
	public IntegerProperty idClienteProperty(){
		return idCliente;
	}

	public String getUsuario(){
		return usuario.get();
	}
	public void setUsuario(String usuario){
		this.usuario.set(usuario);
	}
	public StringProperty usuarioProperty(){
		return usuario;
	}

	public int getIdUsuario(){
		return idUsuario.get();
	}
	public void setIdUsuario(int idUsuario){
		this.idUsuario.set(idUsuario);
	}
	public IntegerProperty idUsuarioProperty(){
		return idUsuario;
	}
	
	public String getEstado(){
		return estado.get();
	}
	public void setEstado(String estado){
		this.estado.set(estado);
	}
	public StringProperty estadoProperty(){
		return estado;
	}
	
	public int getIdEstado(){
		return idEstado.get();
	}
	public void setIdEstado(int idEstado){
		this.idEstado.set(idEstado);
	}
	public IntegerProperty idEstadoProperty(){
		return idEstado;
	}
	
	public String getFecha(){
		return fecha.get();
	}
	public void setFecha(String fecha){
		this.fecha.set(fecha);
	}
	public StringProperty fechaProperty(){
		return fecha;
	}
}