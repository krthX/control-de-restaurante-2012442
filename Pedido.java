package org.bryampaniagua.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;
	
	/**
		@author BryamPaniagua
		
		Contiene todos los setter y getter para la creacion
		de un nuevo pedido
		Es para una lista observable asi que tendra los 
		metodos para que se pueda actualizar la lista 
		automaticamente
	**/
public class Pedido{
	private IntegerProperty idPedido, cantidadPlatillos, cantidadBebidas;
	private StringProperty  platillo, bebida;
	
	/**
		No recibe parametros pero inicializa todas las variables
		necesarias
	**/
	public Pedido(){
		idPedido = new SimpleIntegerProperty();
		platillo = new SimpleStringProperty();
		bebida = new SimpleStringProperty();
		cantidadPlatillos = new SimpleIntegerProperty();
		cantidadBebidas = new SimpleIntegerProperty();
	}
	/**
		Recibe todos los datos para la creacion de un nuevo pedido
		@param idPedido
		@param cantidadPlatillos
		@param bebida
		@param cantidadBebidas
	**/
	public Pedido(int idPedido, String platillo, int cantidadPlatillos, String bebida, int cantidadBebidas){	
		this.idPedido = new SimpleIntegerProperty(idPedido);	
		this.platillo = new SimpleStringProperty(platillo);		
		this.cantidadPlatillos = new SimpleIntegerProperty(cantidadPlatillos);		
		this.bebida = new SimpleStringProperty(bebida);		
		this.cantidadBebidas = new SimpleIntegerProperty(cantidadBebidas);	
	}
	
	/**
		
	**/
	
	public int getIdPedido(){
		return idPedido.get();
	}
	public void setIdPedido(int idPedido){
		this.idPedido.set(idPedido);
	}
	public IntegerProperty idPedidoProperty(){
		return idPedido;
	}
	
	public int getCantidadBebidas(){
		return cantidadBebidas.get();
	}
	public void setCantidadBebidas(int cantidadBebidas){
		this.cantidadBebidas.set(cantidadBebidas);
	}
	public IntegerProperty cantidadBebidasProperty(){
		return cantidadBebidas;
	}

	public int getCantidadPlatillos(){
		return cantidadPlatillos.get();
	}
	public void setCantidadPlatillos(int cantidadPlatillos){
		this.cantidadPlatillos.set(cantidadPlatillos);
	}
	public IntegerProperty cantidadPlatillosProperty(){
		return cantidadPlatillos;
	}
	
	public String getPlatillo(){
		return platillo.get();
	}
	public void setPlatillo(String platillo){
		this.platillo.set(platillo);
	}
	public StringProperty platilloProperty(){
		return platillo;
	}
	
	public String getBebida(){
		return bebida.get();
	}
	public void setBebida(String bebida){
		this.bebida.set(bebida);
	}
	public StringProperty bebidaProperty(){
		return bebida;
	}
}
