package org.bryampaniagua.utilidades;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.collections.ObservableList;

public class Tabla<E>{
	private TableView<E> tvLista;
	
	public Tabla(){
		tvLista = new TableView<E>();
		tvLista.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
	}
	public void createTable(String type[], String NombreColumna[], String variable[]){
		int i = 0;
		do{
			switch(type[i]){
				case "Integer":
					tableInteger(NombreColumna[i], variable[i]);
				break;
				case "String":
					tableString(NombreColumna[i], variable[i]);
				break;
				case "Long":
					tableLong(NombreColumna[i], variable[i]);
				break;
			}
			i++;
		}while(i < NombreColumna.length);
	}
	public void tableInteger(String columna, String var){
		TableColumn<E, Integer> columnInt = new TableColumn<E, Integer>(columna);
		columnInt.setCellValueFactory(new PropertyValueFactory<E, Integer>(var));
		tvLista.getColumns().add(columnInt);
	}
	public void tableString(String columna, String var){
		TableColumn<E, String> columnString = new TableColumn<E, String>(columna);
		columnString.setCellValueFactory(new PropertyValueFactory<E, String>(var));
		tvLista.getColumns().add(columnString);
	}
	public void tableLong(String columna, String var){
		TableColumn<E, Long> columnLong = new TableColumn<E, Long>(columna);
		columnLong.setCellValueFactory(new PropertyValueFactory<E, Long>(var));
		tvLista.getColumns().add(columnLong);
	}
	public void addItems(ObservableList<E> items){
		tvLista.setItems(items);
	}
	public TableView<E> getTable(){
		return this.tvLista;
	}
	public E getSelectedIt(){
		return tvLista.getSelectionModel().getSelectedItem();
	}
}