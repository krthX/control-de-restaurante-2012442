package org.bryampaniagua.manejadores;

import org.bryampaniagua.database.Conexion;
import org.bryampaniagua.beans.Usuario;
import org.bryampaniagua.utilidades.Tabla;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableView;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ManejadorUsuario{
	private Conexion cnx;
	private Usuario enSesion;
	private ObservableList<Usuario> listaDeUsuarios;
	private Tabla<Usuario> tablaUsuario;
	
	public ManejadorUsuario(Conexion cnx){
		this.cnx = cnx;		
		this.listaDeUsuarios = FXCollections.observableArrayList();
	}
	public boolean comprobar(String correo, String pass){
		ResultSet resultado = cnx.execConsulta("SELECT idUsuario, nombre, contrasenia, nick, correo, telefono,edad, mesero, chef, CRUD FROM Usuario WHERE correo = '"+correo+"' AND contrasenia = '"+pass+"' ");
		try{
			if(resultado != null){
				if(resultado.next()){
					enSesion = new Usuario(resultado.getInt("idUsuario"), resultado.getString("nombre"), resultado.getString("nick"), resultado.getString("contrasenia"), resultado.getString("correo"), resultado.getInt("edad"), resultado.getInt("telefono"), resultado.getInt("mesero"), resultado.getInt("chef"), resultado.getInt("CRUD"));
					return true;
				}
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
		return false;
	}
	public Usuario getUsuarioIngresado(){
		return enSesion;
	}
	public void salirDeSesion(){
		enSesion = null;
	}
	public void modificarUsuario(Usuario usuario){
		cnx.execSentencia("UPDATE Usuario SET nombre = '"+usuario.getNombre()+"', nick = '"+usuario.getNick()+"', correo = '"+usuario.getCorreo()+"', contrasenia = '"+usuario.getContrasenia()+"' telefono = "+usuario.getTelefono()+", edad = "+usuario.getEdad()+", mesero = "+usuario.getMesero()+", chef = "+usuario.getChef()+", CRUD = "+usuario.getCRUD()+" WHERE idUsuario = "+usuario.getIdUsuario());
		actualizarListaDeUsuarios();
	}
	
	public void actualizarListaDeUsuarios(){
		listaDeUsuarios.clear();
		ResultSet rsUsuarios = cnx.execConsulta("SELECT idUsuario, nombre, contrasenia, nick, correo, telefono,edad, mesero, chef, CRUD FROM Usuario");
		try{
			if(rsUsuarios != null){
				while(rsUsuarios.next()){
					Usuario usuario = new Usuario(rsUsuarios.getInt("idUsuario"), rsUsuarios.getString("nombre"), rsUsuarios.getString("nick"), rsUsuarios.getString("contrasenia"), rsUsuarios.getString("correo"), rsUsuarios.getInt("edad"), rsUsuarios.getInt("telefono"), rsUsuarios.getInt("mesero"), rsUsuarios.getInt("chef"), rsUsuarios.getInt("CRUD"));
					listaDeUsuarios.add(usuario);
				}
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
	}
	public ObservableList<Usuario> getListaDeUsuarios(){
		actualizarListaDeUsuarios();
		return this.listaDeUsuarios;
	}
	public int getOrdenes(int idUsuario){
		System.out.println(idUsuario);
		int noOrdenes = -1;
		ResultSet noOrden = cnx.execConsulta("SELECT idUsuario AS id, COUNT(Orden.idUsuario) AS ordenes FROM Orden GROUP BY Orden.idUsuario");
		try{
			if(noOrden != null){
				while(noOrden.next()){
					if(idUsuario == noOrden.getInt("id")){
						noOrdenes = noOrden.getInt("ordenes");
						System.out.println(noOrdenes);
					}
				}
			}
		}catch(SQLException sqle){
			sqle.printStackTrace();
		}
		return noOrdenes;
	}
	public void createTableUs(){
		if(tablaUsuario == null){
			tablaUsuario = new Tabla<Usuario>();
			String tipos[] = "String,String,String,Integer,Integer,Integer,Integer,Integer,Integer".split(",");
			String columnas[] = "Nombre,Nick,Correo,Telefono,Edad,Mesero,Chef,CRUD".split(",");
			String variables[] = "nombre,nick,correo,telefono,edad,mesero,chef,CRUD".split(",");
			tablaUsuario.createTable(tipos, columnas, variables);
		}
	}
	public TableView<Usuario> addItemsTableUs(){
		createTableUs();
		tablaUsuario.addItems(getListaDeUsuarios());
		return tablaUsuario.getTable();
	}
	public Usuario getSelected(){
		return tablaUsuario.getSelectedIt();
	}
}